"use strict"

window.onload = init;
function init() {
    const estimateCostBtn = document.getElementById("estimateCost");
    estimateCostBtn.onclick = onCostBtnClicked;

}

function onCostBtnClicked () {
    const carRentalPara = document.getElementById("carRentalPara");
    const rentalOptionsPara = document.getElementById("rentalOptionsPara");
    const under25Para = document.getElementById("under25Para");
    const totalDuePara = document.getElementById("totalDuePara");
    const basicRental = 29.99;

    let numOfDays = document.getElementById("numOfDays").value;
    let totalRental = Number((basicRental * numOfDays).toFixed(2));
    let under25Surcharge = getUnder25Surcharge(totalRental);
    let rentalOptions = getRentalOptions();
    let totalCost = (totalRental + rentalOptions + under25Surcharge).toFixed(2);


    carRentalPara.innerHTML = totalRental;
    rentalOptionsPara.innerHTML = rentalOptions;
    under25Para.innerHTML = under25Surcharge;
    totalDuePara.innerHTML = totalCost;

    return false;
}

function getUnder25Surcharge (totalRental) {
    const yesRadio = document.getElementById("yesRadio");
    let surcharge = 0;

    if (yesRadio.checked) {
        surcharge = surcharge + totalRental * 0.30;
    }

    return Number(surcharge.toFixed(2));
}

function getRentalOptions () {
    const rentalOptionsInputs = document.querySelectorAll("input[type=checkbox]");
    let optionsCost = 0;

    rentalOptionsInputs.forEach(function (item, index) {
         if (item.checked) {
                optionsCost += Number(item.value);
         }
    });

    return Number(optionsCost.toFixed(2));
}